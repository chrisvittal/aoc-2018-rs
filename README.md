# Advent Of Code 2018

My solutions to [Advent of Code 2018](https://adventofcode.com/2017) written in
[Rust](https://rust-lang.org).
