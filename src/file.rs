use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;
use std::str::FromStr;

#[inline]
fn to_buf_reader<P: AsRef<Path>>(p: P) -> io::BufReader<File> {
    let f = File::open(p).expect("could not open file");
    io::BufReader::new(f)
}

#[inline]
/// Simple function that collects a file into a `Lines` iterator, panics on
/// any error.
pub fn to_lines<P: AsRef<Path>>(p: P) -> io::Lines<io::BufReader<File>> {
    to_buf_reader(p).lines()
}

/// Gets the first line of the file minus any trailing newline or CRLF;
pub fn first_line<P: AsRef<Path>>(p: P) -> String {
    let mut s = String::new();
    to_buf_reader(p)
        .read_line(&mut s)
        .expect("could not read line");
    if s.ends_with('\n') {
        s.pop();
        if s.ends_with('\r') {
            s.pop();
        }
    }
    s
}

/// Simple function that collects a file into a `Vec<String>`, panics on
/// any error.
pub fn to_strings<P: AsRef<Path>>(p: P) -> Vec<String> {
    to_lines(p).map(|r| r.expect("error in read")).collect()
}

#[inline]
pub fn to_strings_iter<P: AsRef<Path>>(p: P) -> impl Iterator<Item = String> {
    to_lines(p).map(|r| r.expect("could not read line"))
}

pub fn to_single_parsed<F, P>(p: P) -> Vec<F>
where
    P: AsRef<Path>,
    F: FromStr,
{
    to_lines(p)
        .map(|v| {
            let v = v.expect("error in read");
            match v.parse() {
                Ok(vp) => vp,
                _ => panic!("parse error for input: {}", v),
            }
        })
        .collect()
}

/// Converts input into parsed vector
pub fn to_split_parsed<F, P>(p: P) -> Vec<Vec<F>>
where
    P: AsRef<Path>,
    F: FromStr,
{
    to_lines(p)
        .map(|r| r.expect("error in read"))
        .map(|s| {
            s.split_whitespace()
                .map(|v| match v.parse() {
                    Ok(vp) => vp,
                    _ => panic!("parse error for input: {}", v),
                })
                .collect()
        })
        .collect()
}

pub fn to_split_parsed_with<F, P>(p: P, f: fn(char) -> bool) -> impl Iterator<Item = Vec<F>>
where
    P: AsRef<Path>,
    F: FromStr,
{
    to_lines(p)
        .map(|r| r.expect("error in read"))
        .map(move |s| {
            s.split(f)
                .filter(|s| !s.is_empty())
                .map(|v| match v.parse() {
                    Ok(vp) => vp,
                    _ => panic!("parse error for input: {}", v),
                })
                .collect()
        })
}
