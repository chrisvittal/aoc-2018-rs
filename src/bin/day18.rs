use std::io;
use std::io::prelude::*;

use pathfinding::matrix::Matrix;
use rustc_hash::*;

static INPUT: &str = "data/day18";
const MAX_TIME: usize = 1_000_000_000;
const PART_ONE: usize = 10;
const YARD_SIZE: usize = 50;

#[allow(unused)]
fn print_matrix(m: &Matrix<u8>) -> io::Result<()> {
    let s = io::stdout();
    let mut s = s.lock();
    for _ in 0..m.columns {
        write!(s, "=")?;
    }
    writeln!(s)?;
    for i in 0..m.rows {
        let row = &m.as_ref()[i * m.columns..(i + 1) * m.columns];
        s.write_all(row)?;
        writeln!(s)?;
    }
    Ok(())
}

fn main() {
    let input: Vec<u8> = aoc::file::to_strings_iter(INPUT)
        .map(Vec::from)
        .flatten()
        .collect();
    let mut yard = Matrix::from_vec(YARD_SIZE, YARD_SIZE, input);

    let mut m = FxHashMap::default();
    let mut t = 0;
    while t < MAX_TIME {
        t += 1;
        let mut new = Matrix::new_square(YARD_SIZE, b'.');
        for r in 0..yard.rows {
            for c in 0..yard.columns {
                let idx = &(r, c);
                let mut adj_tree = 0;
                let mut adj_lumb = 0;
                for n in yard.neighbours(idx, true) {
                    adj_tree += (yard[&n] == b'|') as u8;
                    adj_lumb += (yard[&n] == b'#') as u8;
                }
                if yard[idx] == b'.' && adj_tree >= 3 {
                    new[idx] = b'|';
                } else if yard[idx] == b'|' {
                    new[idx] = if adj_lumb >= 3 { b'#' } else { b'|' };
                } else if yard[idx] == b'#' && adj_tree >= 1 && adj_lumb >= 1 {
                    new[idx] = b'#';
                }
            }
        }
        yard = new.clone();
        if let Some(t_) = m.insert(new, t) {
            let skip = (MAX_TIME - t) / (t - t_);
            t += skip * (t - t_);
        }
        if t == PART_ONE || t == MAX_TIME {
            let trees = bytecount::count(yard.as_ref(), b'|');
            let lumber = bytecount::count(yard.as_ref(), b'#');
            println!("  {}: {}", 1 + (t == MAX_TIME) as u8, trees * lumber)
        }
    }
}
