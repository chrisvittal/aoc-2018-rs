use rustc_hash::FxHashMap;

// FIXME: Write a real parser
static INPUT: &str = "data/day04-simplified";

#[derive(Clone, Copy)]
enum Event {
    Guard(i16),
    FallAsleep,
    WakeUp,
}

#[derive(Clone, Copy)]
struct GuardEvent {
    minute: u8,
    evnt: Event,
}

impl From<&[i16]> for GuardEvent {
    fn from(d: &[i16]) -> Self {
        assert_eq!(d.len(), 6);
        let ev = match d[5] {
            -1 => Event::FallAsleep,
            -2 => Event::WakeUp,
            v => Event::Guard(v),
        };
        GuardEvent {
            minute: d[4] as u8,
            evnt: ev,
        }
    }
}

#[derive(Default)]
struct State {
    g: i16,
    f: Option<u8>,
    w: Option<u8>,
}

fn main() {
    let data: Vec<Vec<i16>> = aoc::file::to_split_parsed(INPUT);
    let mut data = data.into_iter().map(|b| b[..].into());
    let mut state: State = Default::default();
    let mut map = FxHashMap::<i16, Vec<(u8, u8)>>::default();
    loop {
        match data.next() {
            Some(GuardEvent {
                evnt: Event::Guard(g),
                ..
            }) => {
                state.g = g;
                state.f = None;
                state.w = None;
            }
            Some(GuardEvent {
                evnt: Event::FallAsleep,
                minute,
                ..
            }) => {
                assert!(state.f.is_none());
                state.f = Some(minute);
            }
            Some(GuardEvent {
                evnt: Event::WakeUp,
                minute,
                ..
            }) => {
                assert!(state.w.is_none());
                state.w = Some(minute);
            }
            None => break,
        }
        if state.f.is_some() && state.w.is_some() {
            let e = map.entry(state.g).or_default();
            if let (Some(f), Some(w)) = (state.f, state.w) {
                e.push((f, w));
                state.f = None;
                state.w = None;
            }
        }
    }
    let mut v = map
        .iter()
        .map(|(&k, v)| (k, v.iter().fold(0, |a, &(f, w)| a + (w - f) as usize)))
        .collect::<Vec<_>>();
    v.sort_unstable_by(|(_, v1), (_, v2)| v2.cmp(v1));
    let min = if let Some(val) = map.get(&v[0].0) {
        let mut arr = [0; 60];
        for &(s, e) in val {
            for i in s..e {
                arr[i as usize] += 1;
            }
        }
        let m = arr.iter().max().unwrap();
        arr.iter().position(|j| j == m).unwrap()
    } else {
        panic!();
    };
    println!("  1: {}", v[0].0 as usize * min);
    let mut v = map
        .iter()
        .map(|(&k, v)| {
            (
                k,
                v.iter().fold([0; 60], |mut a, &(f, w)| {
                    for i in f..w {
                        a[i as usize] += 1;
                    }
                    a
                }),
            )
        })
        .map(|(k, a)| {
            let max = a.iter().cloned().max().unwrap();
            let pos = a.iter().position(|&v| v == max).unwrap();
            (k, max, pos)
        })
        .collect::<Vec<_>>();
    v.sort_unstable_by(|(_, m, _), (_, n, _)| n.cmp(m));
    println!("  2: {}", v[0].0 as usize * v[0].2);
}
