use std::io::prelude::*;
use std::str::FromStr;

use rustc_hash::{FxHashMap, FxHashSet};

static INPUT: &str = "data/day07";

#[derive(Clone, Copy)]
struct Edge {
    needed: u8,
    needs: u8,
}

impl FromStr for Edge {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.trim_start_matches("Step ");
        let s = s.trim_end_matches(" can begin.");
        let b: &[u8] = s.as_ref();
        let c: u8 = *b.first().unwrap();
        let d: u8 = *b.last().unwrap();
        Ok(Self {
            needed: c,
            needs: d,
        })
    }
}

fn main() {
    let input: Vec<Edge> = aoc::file::to_single_parsed(INPUT);
    let mut ids = FxHashSet::default();
    let mut deps = FxHashMap::<_, FxHashSet<_>>::default();

    for Edge { needed, needs } in input {
        deps.entry(needs).or_default().insert(needed);
        ids.insert(needs);
        ids.insert(needed);
    }

    let mut done = FxHashSet::default();
    let mut rest: Vec<_> = ids.into_iter().collect();
    let mut ans = Vec::new();
    rest.sort_unstable_by(|a, b| b.cmp(a));

    // pt 1
    {
        let mut rest = rest.clone();
        while !rest.is_empty() {
            for i in (0..rest.len()).rev() {
                if deps.get(&rest[i]).map_or(true, |s| done.is_superset(s)) {
                    ans.push(rest[i]);
                    done.insert(rest[i]);
                    rest.remove(i);
                    break;
                }
            }
        }
        print!("  1: ");
        std::io::stdout().write_all(&ans).expect("could not write");
        println!();
    }

    // pt 2
    {
        let mut wda = [0usize; 5];
        let mut ida = FxHashMap::default();
        let mut time = 0;
        while !rest.is_empty() {
            time = *wda.iter().min().filter(|&&t| t > time).unwrap_or(&time);
            let mut it = (0..rest.len()).rev();
            while let Some(i) = it.next() {
                if if let Some(m) = deps.get(&rest[i]) {
                    m.iter().all(|d| ida.get(d).map_or(false, |&t| t <= time))
                } else {
                    true
                } {
                    for b in wda.iter_mut() {
                        if *b <= time {
                            *b = time + 60 + (rest[i] - b'@') as usize;
                            ida.insert(rest[i], *b);
                            break;
                        }
                    }
                    rest.remove(i);
                    break;
                }
            }
            if it.next().is_none() {
                time = *wda
                    .iter()
                    .filter(|&&t| t > time)
                    .min()
                    .expect("empty array!!!")
            }
        }
        println!("  2: {}", wda.iter().max().unwrap());
    }
}
