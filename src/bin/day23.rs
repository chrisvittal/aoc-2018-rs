use std::cmp::Reverse;
use std::error::Error;

use lazy_static::lazy_static;
use pathfinding::prelude::absdiff;
use regex::Regex;

static INPUT: &str = "data/day23";
type Result<T> = std::result::Result<T, Box<std::error::Error>>;

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
struct Nanobot {
    pos: (i64, i64, i64),
    range: i64,
}

const ZERO: Nanobot = Nanobot {
    pos: (0, 0, 0),
    range: 0,
};

impl std::str::FromStr for Nanobot {
    type Err = Box<Error>;
    fn from_str(s: &str) -> Result<Self> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"-?\d+").unwrap();
        }
        let mut a = [0; 4];
        for (i, m) in RE.find_iter(s).enumerate() {
            a[i] = m.as_str().parse()?;
        }
        Ok(Nanobot {
            pos: (a[0], a[1], a[2]),
            range: a[3],
        })
    }
}

impl Nanobot {
    fn dist(&self, other: &Nanobot) -> i64 {
        absdiff(self.pos.0, other.pos.0)
            + absdiff(self.pos.1, other.pos.1)
            + absdiff(self.pos.2, other.pos.2)
    }

    fn pt_dist(&self, pt: (i64, i64, i64)) -> i64 {
        let nb = Nanobot { pos: pt, range: 0 };
        nb.dist(self)
    }

    fn in_range(&self, other: &Nanobot) -> bool {
        self.dist(other) <= other.range
    }

    fn pt_in_range(&self, pt: (i64, i64, i64)) -> bool {
        let nb = Nanobot { pos: pt, range: 0 };
        nb.in_range(self)
    }
}

fn solve(mut input: Vec<Nanobot>) {
    input.sort_by_key(|nb| Reverse(nb.range));
    let best = input[0];
    let mut sum = 0;
    for nb in &input {
        sum += nb.in_range(&best) as usize;
    }
    println!("  1: {}", sum);

    let mut mult = 1 << 29;
    const DIV: i64 = 2;
    let mut rx = -2..=2;
    let mut ry = -2..=2;
    let mut rz = -2..=2;
    loop {
        let mut best = ZERO.pos;
        let mut best_dist = 0;
        let mbots = input
            .iter()
            .map(|&nb| Nanobot {
                pos: (nb.pos.0 / mult, nb.pos.1 / mult, nb.pos.2 / mult),
                range: nb.range / mult,
            })
            .collect::<Vec<_>>();
        for x in rx {
            for y in ry.clone() {
                for z in rz.clone() {
                    let pt = (x, y, z);
                    let c = mbots.iter().filter(|nb| nb.pt_in_range(pt)).count();
                    if c < best_dist || c == best_dist && ZERO.pt_dist(pt) > ZERO.pt_dist(best) {
                        continue;
                    }
                    best = pt;
                    best_dist = c;
                }
            }
        }
        if mult == 1 {
            println!("  2: {}", ZERO.pt_dist(best));
            break;
        }
        rx = (best.0 - 1) * DIV..=(best.0 + 1) * DIV;
        ry = (best.1 - 1) * DIV..=(best.1 + 1) * DIV;
        rz = (best.2 - 1) * DIV..=(best.2 + 1) * DIV;
        mult /= DIV;
    }
}

fn main() {
    let input: Vec<Nanobot> = aoc::file::to_single_parsed(INPUT);
    solve(input);
}
