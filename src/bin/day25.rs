use std::error::Error;

use lazy_static::*;
use pathfinding::prelude::absdiff;
use regex::Regex;
use rustc_hash::*;

use aoc::unionfind::UnionFind;

type Result<T> = std::result::Result<T, Box<Error>>;

static INPUT: &str = "data/day25";

#[derive(Clone, Copy, Eq, Debug, PartialEq, Hash)]
struct Pt {
    w: i32,
    x: i32,
    y: i32,
    z: i32,
}

impl Pt {
    fn dist(&self, othr: &Pt) -> i32 {
        absdiff(self.w, othr.w)
            + absdiff(self.x, othr.x)
            + absdiff(self.y, othr.y)
            + absdiff(self.z, othr.z)
    }
}

impl std::str::FromStr for Pt {
    type Err = Box<Error>;
    fn from_str(s: &str) -> Result<Self> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"-?\d+").unwrap();
        }
        let pt = RE
            .find_iter(s)
            .map(|m| {
                m.as_str()
                    .parse()
                    .map_err(|e: std::num::ParseIntError| e.into())
            })
            .collect::<Result<Vec<_>>>()?;
        if pt.len() < 4 {
            Err(format!("not enough elements: {:?}", s).into())
        } else {
            Ok(Self {
                w: pt[0],
                x: pt[1],
                y: pt[2],
                z: pt[3],
            })
        }
    }
}

fn main() {
    let input: Vec<Pt> = aoc::file::to_single_parsed(INPUT);
    let mut uf = UnionFind::new(input.len());
    let mut m = FxHashMap::default();

    for (i, p1) in input.into_iter().enumerate() {
        m.insert(p1, i);

        for p2 in m.keys() {
            if p1.dist(p2) <= 3 {
                uf.union(i, m[p2]);
            }
        }
    }
    println!("  1: {}", uf.sets());
    println!("  2: Merry Christmas!");
}
