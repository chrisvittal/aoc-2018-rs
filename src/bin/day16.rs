use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use lazy_static::lazy_static;
use regex::Regex;
use rustc_hash::FxHashSet;

static INPUT: &str = "data/day16";
lazy_static! {
    static ref DFILT: Regex = Regex::new(r"\d+").unwrap();
}

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
enum Either<A, B> {
    Left(A),
    Right(B),
}

impl<A, B> Either<A, B> {
    #[inline]
    fn is_left(&self) -> bool {
        match self {
            Either::Left(_) => true,
            _ => false,
        }
    }

    #[inline]
    fn is_right(&self) -> bool {
        !self.is_left()
    }
}

#[derive(Eq, PartialEq, Clone, Copy, Hash, Debug)]
#[repr(u8)]
enum OpCode {
    Addr,
    Addi,
    Mulr,
    Muli,
    Banr,
    Bani,
    Borr,
    Bori,
    Setr,
    Seti,
    Gtir,
    Gtri,
    Gtrr,
    Eqir,
    Eqri,
    Eqrr,
}

const OPCODES: [OpCode; 16] = [
    OpCode::Addr,
    OpCode::Addi,
    OpCode::Mulr,
    OpCode::Muli,
    OpCode::Banr,
    OpCode::Bani,
    OpCode::Borr,
    OpCode::Bori,
    OpCode::Setr,
    OpCode::Seti,
    OpCode::Gtir,
    OpCode::Gtri,
    OpCode::Gtrr,
    OpCode::Eqir,
    OpCode::Eqri,
    OpCode::Eqrr,
];

struct Device {
    regs: [usize; 4],
    /// Map between opcode numbers and actual opcodes
    opcodes: [OpCode; 16],
}

fn solve_opcodes(mut map: Vec<Either<OpCode, FxHashSet<OpCode>>>) -> [OpCode; 16] {
    assert_eq!(map.len(), 16);
    while map.iter().any(Either::is_right) {
        for i in 0..map.len() {
            let op = match &map[i] {
                Either::Left(op) => *op,
                _ => continue,
            };
            for (j, m) in map.iter_mut().enumerate() {
                if j == i {
                    continue;
                }
                if let Either::Right(ref mut m) = m {
                    m.remove(&op);
                }
                let op = if let Either::Right(m) = &m {
                    if m.len() == 1 {
                        *m.iter().next().unwrap()
                    } else {
                        continue;
                    }
                } else {
                    continue;
                };
                *m = Either::Left(op);
            }
        }
    }

    let mut ops = OPCODES;
    for (op, m) in ops.iter_mut().zip(map) {
        *op = if let Either::Left(op) = m {
            op
        } else {
            panic!()
        };
    }
    ops
}

impl Device {
    fn raw_op(&mut self, op: OpCode, a: usize, b: usize, c: usize) {
        use self::OpCode::*;
        match op {
            Addr => self.regs[c] = self.regs[a] + self.regs[b],
            Addi => self.regs[c] = self.regs[a] + b,
            Mulr => self.regs[c] = self.regs[a] * self.regs[b],
            Muli => self.regs[c] = self.regs[a] * b,
            Banr => self.regs[c] = self.regs[a] & self.regs[b],
            Bani => self.regs[c] = self.regs[a] & b,
            Borr => self.regs[c] = self.regs[a] | self.regs[b],
            Bori => self.regs[c] = self.regs[a] | b,
            Setr => self.regs[c] = self.regs[a],
            Seti => self.regs[c] = a,
            Gtir => self.regs[c] = (a > self.regs[b]) as usize,
            Gtri => self.regs[c] = (self.regs[a] > b) as usize,
            Gtrr => self.regs[c] = (self.regs[a] > self.regs[b]) as usize,
            Eqir => self.regs[c] = (a == self.regs[b]) as usize,
            Eqri => self.regs[c] = (self.regs[a] == b) as usize,
            Eqrr => self.regs[c] = (self.regs[a] == self.regs[b]) as usize,
        }
    }

    fn op(&mut self, opid: usize, a: usize, b: usize, c: usize) {
        self.raw_op(self.opcodes[opid], a, b, c);
    }

    fn solve<R>(mut rdr: R) -> (Self, usize, R)
    where
        R: BufRead,
    {
        let mut map: Vec<Either<OpCode, FxHashSet<OpCode>>> =
            vec![Either::Right(FxHashSet::default()); 16];
        let mut p1 = 0;
        let mut input = String::new();
        loop {
            input.clear();
            while let Ok(n) = rdr.read_line(&mut input) {
                if n < 3 {
                    break;
                }
            }
            if input.len() < 3 {
                break;
            }
            let v: Vec<usize> = DFILT
                .find_iter(&input)
                .filter_map(|m| m.as_str().parse().ok())
                .collect();
            let test = Test::from_slice(&v);
            if test.run(&mut map) >= 3 {
                p1 += 1;
            }
        }

        let dev = Device {
            regs: [0; 4],
            opcodes: solve_opcodes(map),
        };
        (dev, p1, rdr)
    }

    fn run<R>(&mut self, mut rdr: R)
    where
        R: BufRead,
    {
        let mut l = String::new();
        while let Ok(n) = rdr.read_line(&mut l) {
            if n == 0 {
                break;
            }
            if l.trim().is_empty() {
                continue;
            }
            let mut ds = DFILT.find_iter(&l).filter_map(|m| m.as_str().parse().ok());
            let op = ds.next().expect("None for op");
            let a = ds.next().expect("None for first arg");
            let b = ds.next().expect("None for sencond arg");
            let c = ds.next().expect("None for dest arg");
            self.op(op, a, b, c);
            l.clear();
        }
    }
}

#[derive(Default, Debug)]
struct Test {
    before: [usize; 4],
    oper: [usize; 4],
    after: [usize; 4],
}

impl Test {
    fn from_slice(s: &[usize]) -> Self {
        assert_eq!(s.len(), 12);
        let mut before = [0; 4];
        let mut oper = [0; 4];
        let mut after = [0; 4];
        before.copy_from_slice(&s[0..4]);
        oper.copy_from_slice(&s[4..8]);
        after.copy_from_slice(&s[8..12]);
        Test {
            before,
            oper,
            after,
        }
    }

    fn run(self, map: &mut [Either<OpCode, FxHashSet<OpCode>>]) -> usize {
        let mut dev = Device {
            regs: self.before,
            opcodes: OPCODES,
        };
        let mut candidates = Vec::new();
        for &op in &OPCODES {
            dev.regs = self.before;
            dev.raw_op(op, self.oper[1], self.oper[2], self.oper[3]);
            if dev.regs == self.after {
                candidates.push(op);
            }
        }
        if candidates.len() == 1 {
            map[self.oper[0]] = Either::Left(candidates[0]);
        } else if let Either::Right(ref mut m) = map[self.oper[0]] {
            for &op in &candidates {
                m.insert(op);
            }
        }

        candidates.len()
    }
}

fn main() {
    let input = File::open(INPUT).expect("could not open file");
    let input = BufReader::new(input);
    let (mut dev, p1, remaining) = Device::solve(input);
    println!("  1: {}", p1);
    dev.run(remaining);
    println!("  2: {}", dev.regs[0]);
}
