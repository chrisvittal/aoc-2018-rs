static INPUT: &str = "data/day14";

fn main() {
    let input: usize = aoc::file::first_line(INPUT).parse().unwrap();
    let inarr: Vec<u8> = format!("{}", input).bytes().map(|b| b - b'0').collect();
    let mut st: Vec<u8> = vec![3, 7];
    let mut e1 = 0;
    let mut e2 = 1;
    while st.len() < input + 10 {
        let n = st[e1] + st[e2];
        if n / 10 > 0 {
            st.push(n / 10)
        }
        st.push(n % 10);
        e1 = (e1 + st[e1] as usize + 1) % st.len();
        e2 = (e2 + st[e2] as usize + 1) % st.len();
    }
    print!("  1: ");
    for i in &st[input..input + 10] {
        print!("{}", i);
    }
    println!();
    // reset for p2
    st.clear();
    st.push(3);
    st.push(7);
    let mut e1 = 0;
    let mut e2 = 1;
    let mut p = 0;
    let l = 'out: loop {
        let n = st[e1] + st[e2];
        if n / 10 > 0 {
            st.push(n / 10)
        }
        st.push(n % 10);
        e1 = (e1 + st[e1] as usize + 1) % st.len();
        e2 = (e2 + st[e2] as usize + 1) % st.len();
        while p + inarr.len() < st.len() {
            //println!("{:15}: {:?}", p, &st[p..p+INARR.len()]);
            if inarr == &st[p..p + inarr.len()] {
                break 'out p;
            }
            p += 1;
        }
    };
    println!("  2: {}", l);
}
