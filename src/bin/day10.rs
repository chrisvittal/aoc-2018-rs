use std::ops::*;

use rustc_hash::*;

static INPUT: &str = "data/day10";

#[derive(Clone, Copy, Hash, PartialEq, Eq)]
struct Point<T>(T, T);

impl<T: AddAssign> AddAssign for Point<T> {
    fn add_assign(&mut self, other: Self) {
        self.0 += other.0;
        self.1 += other.1;
    }
}

fn main() {
    let mut input = aoc::file::to_split_parsed_with(INPUT, |c| !char::is_numeric(c) && c != '-')
        .map(|v| (Point(v[0], v[1]), Point(v[2], v[3])))
        .collect::<Vec<_>>();

    for i in 1.. {
        let mut xp = (std::i32::MAX, std::i32::MIN);
        let mut yp = (std::i32::MAX, std::i32::MIN);

        for &mut (ref mut pos, ref vel) in &mut input {
            *pos += *vel;

            xp.0 = i32::min(pos.0, xp.0);
            xp.1 = i32::max(pos.0, xp.1);
            yp.0 = i32::min(pos.1, yp.0);
            yp.1 = i32::max(pos.1, yp.1);
        }

        if yp.1 - yp.0 != 9 {
            continue;
        }

        let set = input.iter().map(|t| t.0).collect::<FxHashSet<_>>();

        println!("  1:");
        for y in yp.0..=yp.1 {
            print!("\t");
            for x in xp.0..=xp.1 {
                if set.contains(&Point(x, y)) {
                    print!("#");
                } else {
                    print!(" ");
                }
            }
            println!();
        }

        println!("  2: {}", i);
        break;
    }
}
