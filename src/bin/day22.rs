use pathfinding::prelude::{absdiff, astar};
use regex::Regex;

type Result<T> = std::result::Result<T, Box<std::error::Error>>;

static INPUT: &str = "data/day22";
const MOD: usize = 20183;

type Pt = (usize, usize);

struct Cave {
    target: Pt,
    bound: Pt,
    regions: Vec<Vec<Region>>,
}

impl Cave {
    const EXTRA_X: usize = 50;
    const EXTRA_Y: usize = 50;
    fn new(depth: usize, target: Pt) -> Self {
        let mut cave = Self {
            target,
            bound: (target.0 + Self::EXTRA_X, target.1 + Self::EXTRA_Y),
            regions: vec![],
        };
        for x in 0..=target.0 + Self::EXTRA_X {
            cave.regions.push(vec![]);
            for y in 0..=target.1 + Self::EXTRA_Y {
                let gidx = cave.gidx((x, y));
                let elvl = (gidx + depth) % MOD;
                cave.regions[x].push(elvl.into());
            }
        }
        cave
    }

    fn gidx(&self, pt: Pt) -> usize {
        match pt {
            (0, 0) => 0,
            _ if pt == self.target => 0,
            (x, 0) => x * 16807,
            (0, y) => y * 48271,
            (x, y) => self.elvl((x - 1, y)) * self.elvl((x, y - 1)),
        }
    }

    fn elvl(&self, (x, y): Pt) -> usize {
        self.regions[x][y].elvl
    }

    fn typ(&self, (x, y): Pt) -> Type {
        self.regions[x][y].typ
    }

    fn target_risk(&self) -> usize {
        let mut sum = 0;
        for x in 0..=self.target.0 {
            for y in 0..=self.target.1 {
                sum += self.typ((x, y)) as usize;
            }
        }
        sum
    }

    fn neighbors(&self, (x, y): Pt) -> impl Iterator<Item = Pt> + '_ {
        static NEIGHBORS: [(i64, i64); 4] = [(1, 0), (0, 1), (-1, 0), (0, -1)];
        NEIGHBORS
            .iter()
            .filter(move |(dx, dy)| {
                !((x == 0 && *dx < 0)
                    || (y == 0 && *dy < 0)
                    || (x == self.bound.0 && *dx > 0)
                    || (y == self.bound.1 && *dy > 0))
            })
            .map(move |(dx, dy)| ((x as i64 + dx) as usize, (y as i64 + dy) as usize))
    }

    fn fastest_path(&self) -> Result<usize> {
        let (_, cost) = astar(
            &(0, 0, Tool::Torch),
            |&(x, y, tool)| {
                self.neighbors((x, y))
                    .filter(move |&pt| self.typ(pt) != tool)
                    .map(move |(nx, ny)| ((nx, ny, tool), 1))
                    .chain(std::iter::once((
                        (x, y, tool.other_valid(self.typ((x, y)))),
                        7,
                    )))
            },
            |&(x, y, _)| absdiff(x, self.target.0) + absdiff(y, self.target.1),
            |&(x, y, tool)| (x, y) == self.target && tool == Tool::Torch,
        )
        .ok_or_else(|| format!("no path found to target {:?}", self.target))?;
        Ok(cost)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
struct Region {
    typ: Type,
    elvl: usize,
}

impl From<usize> for Region {
    fn from(elvl: usize) -> Region {
        Region {
            typ: elvl.into(),
            elvl,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
#[repr(u8)]
enum Type {
    Rocky = 0,
    Wet = 1,
    Narrow = 2,
}

// We set these up such that if Type == Tool, then it CANNOT be used
// in that region
#[derive(Debug, PartialEq, Eq, Clone, Copy, PartialOrd, Ord, Hash)]
#[repr(u8)]
enum Tool {
    None = 0,
    Torch = 1,
    Gear = 2,
}

fn main() -> Result<()> {
    let re = Regex::new(r"\d+")?;
    let s = std::fs::read_to_string(INPUT)?;
    let mut dat = [0; 3];
    for (i, m) in re.find_iter(&s).enumerate() {
        dat[i] = m.as_str().parse()?;
    }
    let cave = Cave::new(dat[0], (dat[1], dat[2]));
    println!("  1: {}", cave.target_risk());
    match cave.fastest_path() {
        Ok(n) => {
            println!("  2: {}", n);
            Ok(())
        }
        Err(e) => {
            eprintln!("{}", e);
            Err(e)
        }
    }
}

impl Tool {
    fn other_valid(self, typ: Type) -> Tool {
        use self::{Tool::*, Type::*};
        match (typ, self) {
            (Rocky, Torch) => Gear,
            (Rocky, Gear) => Torch,
            (Wet, None) => Gear,
            (Wet, Gear) => None,
            (Narrow, None) => Torch,
            (Narrow, Torch) => None,
            _ => panic!("invalid type {:?} for tool {:?}", typ, self),
        }
    }
}

impl PartialEq<Tool> for Type {
    #[inline(always)]
    fn eq(&self, t: &Tool) -> bool {
        *self as u8 == *t as u8
    }
}

impl PartialEq<Type> for Tool {
    #[inline(always)]
    fn eq(&self, t: &Type) -> bool {
        t == self
    }
}

impl From<usize> for Type {
    fn from(n: usize) -> Type {
        match n % 3 {
            0 => Type::Rocky,
            1 => Type::Wet,
            2 => Type::Narrow,
            _ => unreachable!(),
        }
    }
}
