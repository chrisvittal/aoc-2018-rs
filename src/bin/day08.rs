static INPUT: &str = "data/day08";

fn compute(data: &[usize]) -> (&[usize], usize, usize) {
    assert!(data.len() >= 2);
    let nc = data[0];
    let nm = data[1];
    let mut data = &data[2..];
    let mut meta_sum = 0;

    let mut children = Vec::with_capacity(nc);

    for _ in 0..nc {
        let (next, ms, val) = compute(data);
        data = next;
        meta_sum += ms;
        children.push(val);
    }

    let md = &data[..nm];
    let v = md.iter().sum();
    meta_sum += v;
    let value = if nc == 0 {
        v
    } else {
        md.iter()
            .filter(|&&m| m > 0 && m <= children.len())
            .fold(0, |a, &m| children[m - 1] + a)
    };

    (&data[nm..], meta_sum, value)
}

fn main() {
    let input: Vec<usize> = aoc::file::first_line(INPUT)
        .split_whitespace()
        .map(|v| v.parse().unwrap())
        .collect();
    let (_, meta_sum, value) = compute(&input);
    println!("  1: {}", meta_sum);
    println!("  2: {}", value);
}
