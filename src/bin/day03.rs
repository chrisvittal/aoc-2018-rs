use std::str::FromStr;

use lazy_static::lazy_static;

use regex::Regex;

static INPUT: &str = "data/day03";
const ARSIZ: usize = 1200; // Assume the 1200 x 1200 is big enough

#[derive(Clone, Copy)]
struct Claim {
    no: usize,
    x: usize,
    y: usize,
    xl: usize,
    yl: usize,
}

impl FromStr for Claim {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let toint = |m: &regex::Captures, i| m.get(i).unwrap().as_str().parse::<usize>().unwrap();
        lazy_static! {
            static ref RE: Regex = Regex::new(r"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)").unwrap();
        }
        let caps = RE.captures(s).unwrap();
        let no = toint(&caps, 1);
        let x = toint(&caps, 2);
        let y = toint(&caps, 3);
        let xl = toint(&caps, 4);
        let yl = toint(&caps, 5);
        Ok(Self { no, x, y, xl, yl })
    }
}

fn main() {
    let v: Vec<Claim> = aoc::file::to_single_parsed(INPUT);
    let mut m = vec![[0; ARSIZ]; ARSIZ];
    for &Claim { x, y, xl, yl, .. } in &v {
        for i in 0..xl {
            for j in 0..yl {
                m[x + i][y + j] += 1;
            }
        }
    }
    let sum: usize = m.iter().map(|r| r.iter().filter(|&&x| x > 1).count()).sum();
    println!("  1: {}", sum);
    println!("  2: {}", find_claim(&m, &v));
}

fn find_claim(map: &[[u16; ARSIZ]], claims: &[Claim]) -> usize {
    'outer: for &Claim { no, x, y, xl, yl } in claims {
        if map[x][y] != 1 {
            continue;
        }
        for i in 0..xl {
            for j in 0..yl {
                if map[x + i][y + j] != 1 {
                    continue 'outer;
                }
            }
        }
        return no;
    }
    panic!("didn't find value");
}
