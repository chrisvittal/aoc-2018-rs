use std::collections::VecDeque;

const PLAYERS: usize = 424;
const LAST: usize = 71144;

struct Game<'a> {
    state: VecDeque<usize>,
    players: &'a mut [usize],
}

impl Game<'_> {
    fn reset_state(&mut self) {
        self.state.clear();
        self.state.push_back(0);
        for a in self.players.iter_mut() {
            *a = 0;
        }
    }

    fn run_game(mut self, end: usize) -> Result<usize, &'static str> {
        self.reset_state();
        for i in 1..=end {
            if i % 23 == 0 {
                self.players[i % self.players.len()] += i;
                for _ in 0..7 {
                    let b = self.state.pop_back().ok_or("empty container")?;
                    self.state.push_front(b);
                }
                self.players[i % self.players.len()] +=
                    self.state.pop_front().ok_or("empty container")?;
            } else {
                for _ in 0..2 {
                    let f = self.state.pop_front().ok_or("empty container")?;
                    self.state.push_back(f);
                }
                self.state.push_front(i);
            }
        }
        self.players
            .iter()
            .max()
            .ok_or("empty container")
            .map(|r| *r)
    }
}

fn main() -> Result<(), &'static str> {
    let mut scores = [0; PLAYERS];
    let g = Game {
        state: VecDeque::with_capacity(LAST),
        players: &mut scores[..],
    };
    println!("  1: {}", g.run_game(LAST)?);
    let g = Game {
        state: VecDeque::with_capacity(LAST * 100),
        players: &mut scores[..],
    };
    println!("  2: {}", g.run_game(LAST * 100)?);
    Ok(())
}
