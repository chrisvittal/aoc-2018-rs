use rustc_hash::*;

// FIXME: Parse the input properly
const C1: usize = 1_855_046;
const C2: usize = 65_899;

fn main() {
    let mut last = 0;
    let mut first = 0;
    let mut seen = FxHashSet::default();
    let mut r1 = 0;
    loop {
        let mut r2 = r1 | 65536;
        r1 = C1;
        while r2 > 0 {
            r1 = (((r1 + (r2 & 255)) & 0xFF_FFFF) * C2) & 0xFF_FFFF;
            r2 >>= 8;
        }
        if seen.is_empty() {
            first = r1;
        }
        if !seen.insert(r1) {
            break;
        }
        last = r1;
    }
    println!("  1: {}", first);
    println!("  2: {}", last);
}
