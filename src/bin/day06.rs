use rustc_hash::FxHashMap;

static INPUT: &str = "data/day06";
const REGION_DIST: isize = 10000;

fn main() {
    let input: Vec<(isize, isize)> = aoc::file::to_lines(INPUT)
        .map(|l| l.expect("error in read"))
        .map(|l| {
            let mut s = l.split(", ");
            let p = s.next().unwrap().parse().unwrap();
            let q = s.next().unwrap().parse().unwrap();
            (p, q)
        })
        .collect();
    // These mins and maxes are good enough for my input
    let min_x = input.iter().min_by_key(|a| a.0).expect("no elements").0 - 1;
    let max_x = input.iter().max_by_key(|a| a.0).expect("no elements").0 + 1;
    let min_y = input.iter().min_by_key(|a| a.1).expect("no elements").1 - 1;
    let max_y = input.iter().max_by_key(|a| a.1).expect("no elements").1 + 1;
    let mut map = FxHashMap::default();
    map.reserve(((max_x - min_x) * (max_y - min_y)) as usize);
    let mut region_size = 0;
    for x in min_x..=max_x {
        for y in min_y..=max_y {
            let mut closest = None;
            let mut min_dist = std::isize::MAX;
            let mut dist_total = 0;
            for &p in &input {
                let dist = (p.0 - x).abs() + (p.1 - y).abs();
                dist_total += dist;
                if dist < min_dist {
                    closest = Some(p);
                    min_dist = dist;
                } else if dist == min_dist {
                    closest = None;
                }
            }
            map.insert((x, y), closest);
            if dist_total < REGION_DIST {
                region_size += 1;
            }
        }
    }

    let mut revmap = FxHashMap::default();
    for (k, v) in map.iter().filter_map(|(k, v)| {
        if let Some(v_) = v {
            Some((k, v_))
        } else {
            None
        }
    }) {
        let e = revmap.entry(v).or_insert(0);
        if k.0 == min_x || k.0 == max_x || k.1 == min_y || k.1 == max_y {
            *e = std::isize::MIN;
        }
        *e += 1;
    }

    println!("  1: {}", revmap.values().max().expect("no elements"));
    println!("  2: {}", region_size);
}
