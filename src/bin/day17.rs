use std::ops::RangeInclusive;

use lazy_static::lazy_static;
use regex::Regex;
use rustc_hash::FxHashSet;

static INPUT: &str = "data/day17";

struct ClayRange {
    is_vert: bool,
    pos: i64,
    range: RangeInclusive<i64>,
}

impl std::str::FromStr for ClayRange {
    type Err = &'static str;
    fn from_str(s: &str) -> Result<Self, &'static str> {
        lazy_static! {
            static ref PARSE: Regex = Regex::new(r"^(x|y)=(\d+), (x|y)=(\d+)\.\.(\d+)$").unwrap();
        }
        let caps = PARSE.captures(s).expect("string did not match");
        let is_vert = caps.get(1).unwrap().as_str() == "x";
        let pos = caps.get(2).unwrap().as_str().parse().unwrap();
        let range = RangeInclusive::new(
            caps.get(4).unwrap().as_str().parse().unwrap(),
            caps.get(5).unwrap().as_str().parse().unwrap(),
        );
        Ok(Self {
            is_vert,
            pos,
            range,
        })
    }
}

type Pt = (i64, i64);

fn fill(
    flow: &mut FxHashSet<Pt>,
    set: &mut FxHashSet<Pt>,
    clay: &FxHashSet<Pt>,
    max_y: i64,
    x: i64,
    y: i64,
    dir: i8,
) -> bool {
    flow.insert((x, y));

    let bel = (x, y + 1);
    if !clay.contains(&bel) && !flow.contains(&bel) && bel.1 >= 1 && bel.1 <= max_y {
        fill(flow, set, clay, max_y, x, y + 1, 0);
    }

    if !clay.contains(&bel) && !set.contains(&bel) {
        return false;
    }

    let mut lft = (x - 1, y);
    let mut rgt = (x + 1, y);

    let lfill = clay.contains(&lft)
        || !flow.contains(&lft) && fill(flow, set, clay, max_y, lft.0, lft.1, -1);
    let rfill = clay.contains(&rgt)
        || !flow.contains(&rgt) && fill(flow, set, clay, max_y, rgt.0, rgt.1, 1);

    if dir == 0 && lfill && rfill {
        set.insert((x, y));

        while flow.contains(&lft) {
            set.insert(lft);
            lft.0 -= 1;
        }

        while flow.contains(&rgt) {
            set.insert(rgt);
            rgt.0 += 1;
        }
    }

    dir == -1 && (lfill || clay.contains(&lft)) || dir == 1 && (rfill || clay.contains(&rgt))
}

fn main() {
    let input: Vec<ClayRange> = aoc::file::to_single_parsed(INPUT);
    let mut clay = FxHashSet::default();
    for ClayRange {
        is_vert,
        pos,
        range,
    } in input
    {
        for i in range {
            if is_vert {
                clay.insert((pos, i));
            } else {
                clay.insert((i, pos));
            }
        }
    }
    let max_y = clay.iter().max_by_key(|(_, y)| y).expect("empty set").1;
    let min_y = clay.iter().min_by_key(|(_, y)| y).expect("empty set").1;
    let mut flow = FxHashSet::default();
    let mut set = FxHashSet::default();
    fill(&mut flow, &mut set, &clay, max_y, 500, 0, 0);
    println!(
        "  1: {}",
        flow.iter().filter(|p| p.1 >= min_y && p.1 <= max_y).count()
    );
    println!(
        "  2: {}",
        set.iter().filter(|p| p.1 >= min_y && p.1 <= max_y).count()
    );
}
