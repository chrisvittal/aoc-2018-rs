static PATH: &'static str = "data/day02";

fn main() {
    let lines = aoc::file::to_strings(PATH);
    println!("  1: {}", one(&lines));
    println!(
        "  2: {}",
        std::str::from_utf8(&two(&lines)).expect("not utf8")
    );
}

fn one<R: AsRef<[u8]>>(bytes: &[R]) -> u32 {
    let mut two = 0;
    let mut three = 0;
    for b in bytes {
        let (tw, th) = count(b.as_ref());
        two += tw as u32;
        three += th as u32;
    }
    two * three
}

fn count(s: &[u8]) -> (bool, bool) {
    // Assumption, all inputs are lowercase
    let mut freqs = [0; 26];
    for &c in s {
        assert!(c >= b'a' && c <= b'z');
        freqs[(c - b'a') as usize] += 1;
    }
    freqs.iter().fold((false, false), |(two, three), &c| {
        if c == 2 {
            (true, three)
        } else if c == 3 {
            (two, true)
        } else {
            (two, three)
        }
    })
}

fn two<R: AsRef<[u8]>>(bytes: &[R]) -> Vec<u8> {
    let mut ret = Vec::new();
    for (i, a) in bytes.iter().enumerate() {
        'outer: for b in &bytes[(i + 1)..] {
            ret.clear();
            let mut found = false;
            for (c, d) in a.as_ref().iter().zip(b.as_ref()) {
                if c == d {
                    ret.push(*c);
                } else if found {
                    continue 'outer;
                } else {
                    found = true;
                }
            }
            if found {
                return ret;
            }
        }
    }
    panic!("did not find a match")
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_one() {
        static INPUT: &str = r#"abcdef
bababc
abbcde
abcccd
aabcdd
abcdee
ababab
"#;
        let input = INPUT.lines().collect::<Vec<_>>();
        assert_eq!(12, one(&input));
    }

    #[test]
    fn test_two() {
        static INPUT: &str = r#"abcde
fghij
klmno
pqrst
fguij
axcye
wvxyz
"#;
        let input = INPUT.lines().collect::<Vec<_>>();
        let v = two(&input);
        let s = std::str::from_utf8(&v).expect("not utf8");
        assert_eq!(s, "fgij");
    }
}
