use std::ops::{Add, Index, IndexMut, Not};
use std::str::Chars;

use rustc_hash::*;

static INPUT: &str = "data/day20";
const ORIGIN: (i32, i32) = (0, 0);
const DIRS: [Dir; 4] = [Dir::N, Dir::S, Dir::E, Dir::W];

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Dir {
    N,
    S,
    E,
    W,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Default)]
struct Room {
    n: bool,
    s: bool,
    e: bool,
    w: bool,
}

fn build_map(s: &str) -> FxHashMap<(i32, i32), Room> {
    fn fill(chars: &mut Chars, start: (i32, i32), map: &mut FxHashMap<(i32, i32), Room>) {
        let mut pos = start;
        macro_rules! build {
            ($d:ident, $dir:expr) => {{
                let r = map.entry(pos).or_default();
                r[$dir] = true;
                pos = $dir + pos;
                let r = map.entry(pos).or_default();
                r[!$dir] = true;
            }};
        }
        while let Some(c) = chars.next() {
            match c {
                'N' => build!(n, Dir::N),
                'S' => build!(s, Dir::S),
                'E' => build!(e, Dir::E),
                'W' => build!(w, Dir::W),
                ')' => return,
                '|' => pos = start,
                '(' => fill(chars, pos, map),
                _ => {}
            }
        }
    }
    let mut map = FxHashMap::default();
    let mut chars = s.chars();
    fill(&mut chars, ORIGIN, &mut map);
    map
}

fn solve(input: &str, start: (i32, i32)) -> (usize, usize) {
    let map = build_map(input);
    let mut seen = FxHashSet::default();
    seen.insert(start);
    let mut front = FxHashSet::default();
    front.insert(start);

    let mut next = FxHashSet::default();
    let mut counts = Vec::new();

    while !front.is_empty() {
        counts.push(seen.len());
        for p in front.drain() {
            for &dir in DIRS.iter() {
                if map[&p][dir] && seen.insert(dir + p) {
                    next.insert(dir + p);
                }
            }
        }
        std::mem::swap(&mut next, &mut front);
        next.clear();
    }
    (
        counts.len() - 1,
        if let Some(n) = counts.get(1000 - 1) {
            counts.last().unwrap() - n
        } else {
            0
        },
    )
}

fn main() {
    let input = aoc::file::first_line(INPUT);
    let (p1, p2) = solve(&input, ORIGIN);
    println!("  1: {}", p1);
    println!("  2: {}", p2);
}

impl Index<Dir> for Room {
    type Output = bool;
    fn index(&self, dir: Dir) -> &bool {
        match dir {
            Dir::N => &self.n,
            Dir::S => &self.s,
            Dir::E => &self.e,
            Dir::W => &self.w,
        }
    }
}

impl IndexMut<Dir> for Room {
    fn index_mut(&mut self, dir: Dir) -> &mut bool {
        match dir {
            Dir::N => &mut self.n,
            Dir::S => &mut self.s,
            Dir::E => &mut self.e,
            Dir::W => &mut self.w,
        }
    }
}

impl Not for Dir {
    type Output = Dir;
    fn not(self) -> Dir {
        match self {
            Dir::N => Dir::S,
            Dir::S => Dir::N,
            Dir::E => Dir::W,
            Dir::W => Dir::E,
        }
    }
}

impl Add<(i32, i32)> for Dir {
    type Output = (i32, i32);
    fn add(self, mut other: Self::Output) -> Self::Output {
        match self {
            Dir::N => other.1 -= 1,
            Dir::S => other.1 += 1,
            Dir::E => other.0 += 1,
            Dir::W => other.0 -= 1,
        }
        other
    }
}
