static INPUT: &str = "data/day05";

fn react<'a>(input: impl Iterator<Item = &'a u8>) -> usize {
    let mut v = Vec::new();
    for &c in input {
        match v.last() {
            None => v.push(c),
            Some(&d) => {
                if d.to_ascii_lowercase() == c.to_ascii_lowercase() && d != c {
                    v.pop();
                } else {
                    v.push(c);
                }
            }
        }
    }
    v.len()
}

fn main() {
    let input: Vec<u8> = aoc::file::first_line(INPUT).into();
    println!("  1: {}", react(input.iter()));
    let mut min = std::usize::MAX;
    for i in 0u8..=26 {
        let v = input.iter().filter(|&&c| c != b'a' + i && c != b'A' + i);
        min = usize::min(react(v), min);
    }
    println!("  2: {}", min);
}
