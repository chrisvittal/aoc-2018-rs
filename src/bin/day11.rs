use itertools::iproduct;

static INPUT: &str = "data/day11";
const LENGTH: usize = 300;

fn solve(serial_number: i32, min_size: usize, max_size: usize) -> (usize, usize, usize) {
    let power = (1..=LENGTH)
        .map(|y| {
            (1..=LENGTH)
                .map(|x| {
                    let rack_id = x as i32 + 10;
                    (((rack_id * y as i32 + serial_number) * rack_id / 100) % 10) - 5
                })
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();
    let transposed = (0..LENGTH)
        .map(|x| (0..LENGTH).map(|y| power[y][x]).collect::<Vec<_>>())
        .collect::<Vec<_>>();
    let mut r = Vec::with_capacity(max_size - min_size + 1);
    let mut powers = power.clone();
    for size in 1..=max_size {
        if size >= min_size {
            r.push(
                iproduct!(1..=301 - size, 1..=301 - size)
                    .map(|(x, y)| (powers[y - 1][x - 1], x, y))
                    .max()
                    .map(|(t, x, y)| (t, x, y, size))
                    .unwrap(),
            );
        }
        for (x, y) in iproduct!(1..=LENGTH - size, 1..=LENGTH - size) {
            powers[y - 1][x - 1] += power[y - 1 + size][x - 1..=x - 1 + size]
                .iter()
                .sum::<i32>()
                + transposed[x - 1 + size][y - 1..y - 1 + size]
                    .iter()
                    .sum::<i32>();
        }
    }
    r.into_iter().max().map(|(_, x, y, s)| (x, y, s)).unwrap()
}

fn main() {
    let input = aoc::file::first_line(INPUT).parse().unwrap();
    let (x, y, _) = solve(input, 3, 3);
    println!("  1: {},{}", x, y);
    let (x, y, w) = solve(input, 1, LENGTH);
    println!("  2: {},{},{}", x, y, w);
}
