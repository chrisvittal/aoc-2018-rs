use rustc_hash::FxHashSet;

static PATH: &'static str = "data/day01";

fn main() {
    let v = aoc::file::to_single_parsed(PATH);
    println!("  1: {}", v.iter().sum::<isize>());
    println!("  2: {}", two(&v));
}

fn two(its: &[isize]) -> isize {
    let mut s = FxHashSet::default();
    s.insert(0);
    let mut sum = 0;
    loop {
        for i in its {
            sum += i;
            if !s.insert(sum) {
                return sum;
            }
        }
    }
}
